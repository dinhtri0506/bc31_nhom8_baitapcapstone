document.getElementById("switchButton").onclick = function () {
  document.getElementById("myBody").classList.toggle("dark");
};
function toggleIcon() {
  document
    .getElementsByClassName("header-navbar-toggle-icon")[0]
    .classList.toggle("change");
}

// Carousel BS4 setting
$(".carousel").carousel({
  interval: 6000,
});

// Video
window.addEventListener("scroll", videoSetting);
function videoSetting() {
  var videoEl = document.getElementById("poochcare-video");
  if (document.documentElement.scrollTop < 1400) {
    videoEl.classList.remove("videoRelative");
  } else {
    videoEl.classList.add("videoRelative");
    videoEl.classList.remove("hide");
  }
}
document.getElementById("close").addEventListener("click", closeVideo);
function closeVideo() {
  var poochcareVideo = document.getElementById("poochcare-video");
  poochcareVideo.classList.add("hide");
}

// Back to top
window.addEventListener("scroll", showButton);
function showButton() {
  var backToTop = document.getElementById("btn-back-to-top");
  if (document.documentElement.scrollTop > 200) {
    if (document.documentElement.scrollTop > 300) {
      backToTop.style.display = "flex";
      backToTop.style.opacity = 1;
    } else {
      backToTop.style.display = "flex";
      backToTop.style.opacity = 0;
    }
  } else {
    backToTop.style.display = "none";
  }
}
